import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   // ulesanna - yl8
   // ulesanna - taiendgraaf aka complement graph
   // how to use
   // create new graphs in public void run method and then nameofGraph.createRandomSimpleGraph(amount of vertexes, amount of edges)
   // After that the method will do everything for you and will print out the first graph and then complement graph for it. 
   // It will actually display first graph -> complete graph -> complement graph. 
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (7, 10);
  
   }

   // https://en.wikipedia.org/wiki/Complement_graph
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
     

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }


   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
  

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

 
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;


      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }
      
       
      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }
      
      public Arc deleteArc (String aid, Vertex from, Vertex to){
		Arc res = new Arc (aid);
		res = null;
    	return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
       int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
        System.out.print(this);
//        for(int i=0; i<n; i++) { 
//     	   for(int j=0; j<n; j++)
//     	   System.out.print(connected[i][j] + " "); 
//     	   System.out.println();
   
//       }
       System.out.println("Complement Graph Below");
       int i;
       int j;
       for(i=0; i<n; i++) { 
    	   for(j=0; j<n;j++){
               if (i==j || j==i) 
                   continue;  
               if (connected [i][j] != 0 || connected [j][i] != 0) 
                   continue;  
    		   if (connected [i][j] == 0 || connected [j][i] == 0) {
    Vertex vi = vert[i];
   	Vertex vj = vert[j];
        createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
      connected [i][j] = 2;
        createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
      connected [i][j] = 2;
            }
      }
       }

       System.out.print(this);
 //      for(i=0; i<n; i++) { 
 //   	   for(j=0; j<n; j++)
 //   	   System.out.print(connected[i][j] + " "); 
 //   	   System.out.println();
  
 //     }
       
       for(i=0; i<n; i++) { 
    	   for(j=0; j<n;j++){
               if (i==j || j==i) 
                   continue;  // no loops
               if (connected [i][j] != 1 || connected [j][i] != 1) 
                   continue;  // no multiple edges
    		   if (connected [i][j] == 1 || connected [i][j] == 1) {
    			    Vertex vi = vert[i];
    			   	Vertex vj = vert[j];
    			   	String t = "a" + vi.toString() + "_" + vj.toString();
    			   	deleteArc(t,vi,vj);
    			   	connected [i][j]=0;
    			   	String d = "a" + vj.toString() + "_" + vi.toString();
    			   	deleteArc(d,vj,vi);
    			   	connected [j][i]=0;
    		   }
    	   }
       }
       
       

   }   
}
}